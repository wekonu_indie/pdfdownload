﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PdfViewer : MonoBehaviour
{
    private PdfDownloader pdfDownloader;
    private BubbleController bubbleController;

    [SerializeField] Button openButton;

    private bool isFocus = false;
    private bool isProcessing = false;
    private Text m_debugText;

    private void Start()
    {
        pdfDownloader = this.GetComponent<PdfDownloader>();
        bubbleController = this.GetComponent<BubbleController>();
        openButton.onClick.AddListener(OnOpenButtonClicked);
        m_debugText = pdfDownloader.debugText.GetComponent<Text>();
    }

    //public void OpenPdf()
    //{
    //    string filePath = Application.persistentDataPath + "/" + pdfDownloader.fileName + ".pdf";
    //    if (FileChecker.IsFileExists(filePath))
    //    {
    //        Application.OpenURL(filePath);
    //    }
    //    else
    //    {
    //        bubbleController.PopBubble("Unable to locate the pdf file", true);
    //    }
    //}

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    

    private void OnApplicationFocus(bool focus)
    {
        isFocus = focus;
    }

    public void OnOpenButtonClicked()
    {
        string filePath = Application.persistentDataPath + "/" + pdfDownloader.fileName + ".pdf";
        if (FileChecker.IsFileExists(filePath))
        {
            bubbleController.PopBubble("OnOpenButtonClicked()", true);
            m_debugText.text = filePath;
            OpenPdf(filePath);
        }
        else
        {
            bubbleController.PopBubble("Unable to locate the pdf file.", true);
        }
    }

    private void OpenPdf(string filePath)
    {
#if UNITY_ANDROID
        if (!isProcessing)
        {
            bubbleController.PopBubble("OpenPdf()");
            StartCoroutine(OpenPdfInAndroid());
        }
        else
        {
            bubbleController.PopBubble("Still processing", true);
        }
#else
        if(Debug.isDebugBuild)
        {
            Debug.Log("No sharing set up for this platforn.");
        }
#endif
    }

#if UNITY_ANDROID
    public IEnumerator OpenPdfInAndroid()
    {
        m_debugText.text = "OpenPdfInAndroid";

        string filePath = Application.persistentDataPath + "/" + pdfDownloader.fileName + ".pdf";
        isProcessing = true;
        //wait for graphics to render
        //yield return new WaitForEndOfFrame();
        //ScreenCapture.CaptureScreenshot(pdfDownloader.fileName + ".pdf", 1);
        //yield return new WaitForSeconds(0.5f);

        if (!Application.isEditor)
        {
            m_debugText.text = "Application is not Editor.";
            //get current activity context
            AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

            //create file object of the screenshot captured
            AndroidJavaObject fileOject = new AndroidJavaObject("java.io.File", filePath);

            //create FileProvider class object
            AndroidJavaClass fileProviderClass = new AndroidJavaClass("android.support.v4.content.FileProvider");

            object[] providerParams = new object[3];
            providerParams[0] = currentActivity;
            providerParams[1] = "com.agrawalsuneet.unityclient.provider"; //android:authorities
            providerParams[2] = fileOject;

            //instead of passing the uri, will get the uri from file using FileProvider
            AndroidJavaObject uriObject = fileProviderClass.CallStatic<AndroidJavaObject>("getUriForFile", providerParams);

            //create intent for action view
            AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
            AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
            intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_VIEW"));

            object[] callParams = new object[2];
            callParams[0] = uriObject;
            callParams[1] = "application/pdf";

            m_debugText.text = "App Id: " + Application.identifier;

            //put image and string extra
            //intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
            //intentObject.Call<AndroidJavaObject>("setType", "application/pdf");
            intentObject.Call<AndroidJavaObject>("setDataAndType", callParams);
            //intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), shareSubject);
            //intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), shareMessage);

            //additionally grant permission to read the uri
            intentObject.Call<AndroidJavaObject>("addFlags", intentClass.GetStatic<int>("FLAG_GRANT_READ_URI_PERMISSION"));

            AndroidJavaObject chooser = intentClass.CallStatic<AndroidJavaObject>("createChooser", intentObject, "Open Pdf");

            m_debugText.text = "calling the intent";
            currentActivity.Call("startActivity", chooser);
        }

        yield return new WaitUntil(() => isFocus);
        isProcessing = false;
    }
#endif
}
