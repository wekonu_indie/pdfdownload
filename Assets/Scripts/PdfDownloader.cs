﻿using UnityEngine;
using System.Net;
using System.ComponentModel;
using UnityEngine.UI;

public class PdfDownloader : MonoBehaviour
{
    public string fileName = "thefing";
    [SerializeField] string downloadUri = "https://kayg2a.dm.files.1drv.com/y4msiWaRXjXu2crPBhBUOYh4qstoMgRhtUKfYDbxxUvUAWWtcHycoervFlCHLugw7p7b_FUuJ9eGMiLvMm564ajrGLHoh0xEiQidqTfbd-yHDRn68yu1nlGvCh5MKXiejhr4oDVYm6FaKdmN6ZeJGQUcQOOoFgOdZmFPJ3EnHMCKlrOzHL6mwu5SQSaxNehxqbkWzUaPJ9nKYB3o4r7-zg0_w";
    public GameObject debugText;

    private BubbleController bubbleController;

    private void Start()
    {
        bubbleController = this.GetComponent<BubbleController>();
        debugText.SetActive(Debug.isDebugBuild);
    }

    public void DownloadFile()
    {
        string filePath = Application.persistentDataPath + "/" + fileName + ".pdf";
        if (!FileChecker.IsFileExists(filePath))
        {
            ProcessDownload();
        }
        else
        {
            bubbleController.PopBubble("File already downloaded", true);
        }
    }

    private void ProcessDownload()
    {
        WebClient client = new WebClient();
        client.DownloadFileCompleted += new AsyncCompletedEventHandler(DownloadFileCompleted);
        client.DownloadFileAsync(new System.Uri(downloadUri), Application.persistentDataPath + "/" + fileName + ".pdf");

        bubbleController.PopBubble("DOWNLOAD STARTED");

        if (Debug.isDebugBuild)
        {
            Debug.Log("DOWNLOAD STARTED");
        }
    }

    private void DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
    {
        if (e.Error == null)
        {
            AllDone();
        }
        else
        {
            if (Debug.isDebugBuild)
            {
                Debug.Log("an error occured during file download");
            }

            bubbleController.PopBubble("an error occured during file download. download canceled", true);
        }
    }

    private void AllDone()
    {
        if (Debug.isDebugBuild)
        {
            Debug.Log("DOWNLOAD COMPLETED");
            Debug.Log("File saved in: " + Application.persistentDataPath);
            debugText.GetComponent<Text>().text = Application.persistentDataPath;
        }

        bubbleController.PopBubble("DOWNLOAD COMPLETED", true);
    }
}
