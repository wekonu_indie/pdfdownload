﻿using UnityEngine;
using UnityEngine.UI;

public class BubbleController : MonoBehaviour
{
    [SerializeField] Text bubbleText;
    [SerializeField] GameObject bubble;

    public void PopBubble(string message, bool isPop = false, float duration = 2f)
    {
        bubble.SetActive(true);
        bubbleText.text = message;
        if (isPop)
        {
            Invoke("HideBubble", duration);
        }
    }

    private void HideBubble()
    {
        bubble.SetActive(false);
    }
}
