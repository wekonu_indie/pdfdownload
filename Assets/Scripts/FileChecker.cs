﻿using UnityEngine;
using System.IO;

public static class FileChecker
{
    static long expectedFileSize = 40000;
    public static bool IsFileExists(string filePath)
    {
        if (!File.Exists(filePath))
        {
            if (Debug.isDebugBuild)
            {
                Debug.Log("Path does not exists");
            }
            return false;
        }
        else
        {
            if (Debug.isDebugBuild)
            {
                Debug.Log("Path exists");
            }

            var fileInfo = new System.IO.FileInfo(filePath);
            if(fileInfo.Length <= expectedFileSize)
            {
                if (Debug.isDebugBuild)
                {
                    Debug.Log("File size less than expected.");
                }
                return false;
            }
            else
            {
                if (Debug.isDebugBuild)
                {
                    Debug.Log("File size is as expected.");
                }
            }
            return true;
        }
    }
}
